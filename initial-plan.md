# Practise: Coding interview preparation
Roadmap to prepare for coding interview in 20 weeks. It is divided into two parts. Learn concepts for first 10 weeks and apply these concepts by practising problems from various websites. 

Resources to learn concepts (first 10 weeks):
- Being Zero CCI 25 [course link](http://www.mentorpick.com/course/view/5b41c746850e271c4a1e416b)
- Book: cracking the coding interview
- IIT Kanpur Summer School 
- Dr. Venkatesh Raman Algorithms course
- Professor Gaurav Raj Algorithms course
- [mycodeschool](https://www.youtube.com/user/mycodeschool/playlists)
- [Rachit](https://www.youtube.com/watch?v=gm4Ye0fESpU&list=PLfBJlB6T2eOsET4tlfcLs7oXR7kCyt1xc)
<!-- - https://medium.freecodecamp.org/the-top-data-structures-you-should-know-for-your-next-coding-interview-36af0831f5e3 -->
<!-- - https://www.geeksforgeeks.org/commonly-asked-data-structure-interview-questions-set-1/ -->
<!-- - https://medium.com/@kingrayhan/500-data-structures-and-algorithms-practice-problems-and-their-solutions-b45a83d803f0 -->
<!-- - https://www.indeed.com/prime/resources/talent#interviewing -->
<!-- - https://www.indeed.com/prime/resources/talent/your-complete-guide-to-acing-your-tech-interview-part-1-the-basics -->



#### Schedule for preparation
- **Milestone I**
    - Weeks 01-02: Algorithm Design Techniques
    - Weeks 03-04: Data Structures
    - Week 05: *Buffer / Revision / CoverUp Week*
- **Milestone II**
    - Weeks 06-07: Data Structures using Standard Libraries
    - Weeks 08-09: Standard Algorithms
    - Week 10: *Buffer / Revision / CoverUp Week*
- **Milestone III**
    - Weeks 11-12: Finish remaining questions from 'being zero' and 'cracking the coding interview'
    - Weeks 13-14: algoexpert (65 Q's)
    - Week 15: *Buffer / Revision / CoverUp Week*
- **Milestone IV**
    - Weeks 16-17: Previous interview questions
    - Weeks 18-19: codility + book: top 20 coding interview problems
    - Week 20: *Buffer / Revision / CoverUp Week*

<!-- Images of solved problems and short description for each puzzle -->
#### ToDo in free time
- improve thinking skills 
    - puzzles 
        - [geeksforgeeks](https://www.geeksforgeeks.org/puzzles/)
        - [@codingfreak](https://medium.com/@codingfreak/top-25-programming-puzzles-and-brain-teasers-dac17b41e94a)
        - [crazyforcode](http://www.crazyforcode.com/top-10-interview-puzzles/)
        - from scts
    - xkcd webcomic
        - [website](https://xkcd.com/)
        - explanation [here](https://explainxkcd.com/)
    - basecs 
        - [podcast](https://www.codenewbie.org/basecs "link to podcast")
        - [blog](https://medium.com/basecs "link to website")
    - explain like I am five
- interview preparation resources
    - previous interview experiences from [glassdoor](https://www.glassdoor.ie/Interview/Amazon-Ireland-Interview-Questions-EI_IE6036.0,6_IL.7,14_IN70.htm?filter.jobTitleFTS=software+development)
    - interview topics from jobs [website1](https://www.amazon.jobs/en/landing_pages/software-development-topics), [website2](https://careers.microsoft.com/us/en/interviewtips)

---

### Week 01 (Algorithm Design Techniques)
- iteration
    - basic iteration
        - single loops
        - multiple loops w, w/o nested
        - single iteration variable
    - master loops / flow control
        - pattern printing
    - multiple iteration variables/pointers 
        - two pointer technique
            - sliding window
            - fast, slow pointers
            - pointer from either side
- recursion
    - basic recursion (mostly observing patterns)
        - recursive iteration
        - print recursion calls for the code
        - content before |& after recursive call
        - one variable, multiple variables in recursive call
        - single and multiple recursive calls
        - recursion return types
            - print the result
            - 3 ways to return results recursively
                - Store results to a global variable
                - Store results to a passed variable
                - Build up the results as we return
        - count / print different possibilities
        - printing path in the recursion tree
        - find paths 
        - print paths
        - find final state for each path
        - given a final state, can you find path for it
        - upto one state one path and different path from that state
    - breaking into sub-problems
        - say recursion has 'n' states, try to find (n-1)th state and how it will reach to the nth state
        - look at the desired state and work backwards from there
        - finding sub-problem
        - finding base case, recursive case
    - selections / combinations 
        - with repetations
        - without repetations
    - ordering / permutations 
        - with repetations
        - without repetations
    - recursion pitfalls

### Week 02 (Algorithm Design Techniques)
- recursion
    - divide and conquer
    - backtracking
    - depth first search (DFS)
        - DFS tree
        - DFN
    - branch and bound
    - breadth first search (BFS)
    - dynamic programming
        - top-down or bottom-up
    - greedy algorithms
 
### Week 03 (Data Structures - Basic)
- basic data structures
    - strings
    - arrays
    - vector | dynamic array
        - amortised time complexity in vector
    - lists
        - linked list
        - doubly linked list
        - circular linked list 
        - circular doubly linked list
    - stack
    - queue based 
        - queue
        - dequeue
        - circular queue
        - priority queue
        - binary heap
    - binary tree
        - binary search tree
        - balanced bst
            - rb tree
            - avl tree
    - hashing 
        - collision handling
        - chaining
        - amortised time complexity in hashing
   
### Week 04 (Data Structures - Advanced)
- graphs (source: [codeforces](https://codeforces.com/blog/entry/16221))
- tree based
    - splay tree
    - trie
    - suffix tree
    - b-tree (and it's link to btrfs)
    <!-- - [2-3-4 tree](https://www.educative.io/page/5689413791121408/80001) -->
- segment tree
- disjoint set or union find (source: [hE](https://www.hackerearth.com/practice/notes/disjoint-set-union-union-find/))

### Week 05 (Buffer / Revision / CoverUp Week)
---

### Week 06 (Built in Data Structures - C++ STL)
- string
- simple arrays
- cpp stl (source: [gfg](https://www.geeksforgeeks.org/the-c-standard-template-library-stl/), [codingfreak](https://medium.com/@codingfreak/data-structures-and-algorithms-problems-in-c-using-stl-7900a6ddb1d4))
    - algorithms
    - containers
        - vector
        - list
        - deque
        - arrays
        - forward_list
        - queue
        - priority_queue
        - stack
        - set
        - multiset
        - map
        - multimap
        - unordered_set 
        - unordered_multiset 
        - unordered_map 
        - unordered_multimap
    - functors (NOT Functions!)
    - iterators
    - pair
- problems

### Week 07 (Built in Data Structures - Python3)
- sources: [swaroopch](https://python.swaroopch.com/data_structures.html), [hE](https://www.hackerearth.com/practice/python/working-with-data/lists/tutorial/), [gfg](https://www.geeksforgeeks.org/stack-queue-python-using-module-queue/)
- string
- list
- tuple
- dict
- collections
- set
- stack 
- queue 
- dequeue
- heapq
- problems

### Week 08 (Standard Algorithms)


### Week 09 (Standard Algorithms)


### Week 10 (Buffer / Revision / CoverUp Week)
---

### Week 11
- finish remaining questions from the book 'cracking the coding interview'

### Week 12
- finish remaining problems from being zero cci25

### Week 13
- To learn concepts by solving problems from [algo expert](https://www.algoexpert.io/questions "link to algoexpert 65 questions").

### Week 14
- To learn concepts by solving problems from [algo expert](https://www.algoexpert.io/questions "link to algoexpert 65 questions").


### Week 15 (Buffer / Revision / CoverUp Week)
---

### Week 16
- previous interview questions
    - standard interview questions
        - being zero ([1](https://www.youtube.com/watch?v=LQYYkSe_9CY))
        - glassdoor
        - [Moishe Lettvin](https://www.youtube.com/watch?v=r8RxkpUvxK0)
        - [Irfan Baqui](https://www.youtube.com/channel/UCYvQTh9aUgPZmVH0wNHFa1A/playlists)
        - [csdojo](https://www.youtube.com/watch?v=qli-JCrSwuk&list=PLBZBJbE_rGRVnpitdvpdY9952IsKMDuev)
        - daily coding problem [website](https://www.dailycodingproblem.com), book
    - onion type interview questions
        - [interviewing.io](https://www.youtube.com/channel/UCNc-Wa_ZNBAGzFkYbAHw9eg/videos)
        - gfg
### Week 17
- online white boarding mock interviews

### Week 18
- To learn and get acquainted to code in the coding platform, [codility](https://app.codility.com/programmers/lessons/1-iterations/ "link to codility programmers lessons").

### Week 19
- book: top 20 coding interview problems

### Week 20 (Buffer / Revision / CoverUp Week)
---
