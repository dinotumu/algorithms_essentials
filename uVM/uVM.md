# Micro Virtual Machines and Firecracker

### Contents

1. [How did everything start?](#how-did-everything-start) 
2. [Firecracker Deep Dive](#firecracker-deep-dive)
3. [Firecracker and AWS Lambda](#firecracker-and-aws-lambda)
4. [Firecracker and AWS Fargate](#firecracker-and-aws-fargate)
5. [Key Sources](#keysources)

## How did everything start?

### LP: Invent and Simplify
- Is Virtual Machine secure and fast at the same time? 
- What a virtual machine would look like if it was designed for today’s world of containers and functions?

### Answer: micro Virtual Machine (uVM)
What is Firecracker?
- Firecracker is an open-source virtualization technology which allows to create microVM, or micro virtual machine, that are both secure and fast, and are well suited for serverless computing.
- It's a virtual machine manager, that uses KVM, to create and manage lightweight virtual machines, or uVMs.
- Until now, you needed to choose between containers with fast startup times and high density (or) VMs with strong hardware virtualization-based security and workload isolation. 
- With Firecracker, you no longer have to choose. These uVMs provide enhanced security and workload isolation over traditional VMs, while enabling the speed and resource-efficiency and resource efficiency of containers. 
- It comes with an extremely low resource overhead, that makes it very suitable for serverless computing. 

Why AWS developed it?
- Firecracker was developed at Amazon Web Services to improve the customer experience of services like AWS Lambda and AWS Fargate.
- When Lambda was launched in November 2014, it was providing a secure serverless experience. At launch, EC2 instances were used to provide strong security and isolation between customers workloads. 
- As Lambda grew, a need was felt for technology to provide a highly secure, flexible, and efficient run-time environment for services like Lambda and Fargate. 
- The existing virtualization technologies were not found suitable to optimize for the event driven, sometimes short-lived nature of these kind of workloads. Therefore uVM and firecracker.



## Firecracker Deep Dive

### Firecracker distinctive features
- Firecracker implements a minimal device model, that excludes all non-essential functionality, and reduces the attack surface area of uVM. This improves security, decreases the startup time, and increases hardware utilization. 
- *Security:* 
    - Firecracker uVMs use KVM-based virtualization, that provide enhanced security over traditional VMs. 
    - This ensures that workloads from different end customers can run safely on the same machine. 
    - Minimal Device Model
        - Firecracker provides a minimal required device model to the guest operating system while excluding non-essential functionality and minimizing the attack surface area.
        - There are only 4 emulated devices: virtio-net, virtio-blk, serial console, and a 1-button keyboard controller used only to stop the microVM (reset/restart - to shutdown))
        - In layman terms: a network device, a block I/O device, a serial console, and a partial keyboard (just enough to allow the VM to be reset).
        -  A Programmable Interval Timer, the KVM clock to adjust times between host and uVM
    - Process Jail – The Firecracker process is jailed using cgroups and seccomp BPF, and has access to a small, tightly controlled list of system calls.
    - Static Linking – The firecracker process is statically linked, and can be launched from a jailer to ensure that the host environment is as safe and clean as possible.

- *Faster Startup time:*
    - In addition to a minimal device model, Firecracker also accelerates kernel loading, and provides a minimal Guest kernel configuration. (From the application's perspective, the device interface appears as a set of functions that it can call and through which it can pass data to the kernel and receive data back from it.)
    - The only devices are IOnet and IOblock (check '/dev')
    - The recent pin helps when there is no is no power management device
    - This enables fast startup times. Firecracker initiates user space or application code in less than 125 milliseconds, and supports uVM creation rates of 150 uVM per second, per host. 
    
- *Resource Utilization:*
    - Each Firecracker uVM runs with a reduced memory overhead of less than five MiB, enabling a high density of uVMs to be packed on each server. 
    -  Firecracker provides a rate limiter built into every uVM. This enables optimized sharing of network, and storage resources, even across thousands of uVMs. And, thousands of uVMs, on each host. 
    - Firecracker does paging on demand. So we are only bringing memory as the workload needs it.
    - For example, to run 4000 uVMs of 128MiB each, if we want to pre-allocate the memory, the creation of uVMs would take more time. 
    - Therefore, oversubscription of host CPU and memory is supported natively by default in firecracker
    
### Firecracker Design Principles 
- Multi-tenancy
- vCPU and memory combination
- Oversubscribe CPU and memory
    - The degree of oversubscription is controlled by customers, who may factor in workload correlation and load, in order to ensure smooth host system operation
- Steady mutation rate of five uVMs per host core per second
    - With a uVM configured, with a minimal Linus Kernel, single-code CPU, and 128 megabytes of RAM
    - Five uVMs per host core per second. For example, one can create 180 uVMs per second on a host, with 36 physical cores
- The number of Firecracker uVMs, running simultaneously on a host is limited only by the availability of hardware resources. 
- Each uVM exposes a host-facing API via an in-process HTTP server
- Each uVM provides a guest-facing API access to host-configured metadata via the /mmds API
    - Host configured metadata is also known as MMDS (uVM metadata service)


### Firecracker Design

- The diagram below shows an example host running Firecracker uVMs. 

    ![](/images/fc_design.png)
- The description for the above diagram is shown below:

    Host View | Guest View (RESTful API)
    --- | ---
    Firecracker runs in user space and uses KVM to create uVMs | It enables common actions such as configuring the number of vCPUs or starting the machine
    The fast start up time and low memory overhead of each uVM enables you to pack thousands of uVMs onto the same machine | It provides a built-in rate limiters, which allows you to granularly control network and storage resources used by thousands of microVMs on the same machine
    This means that every function or container group can be encapsulated with the virtual machine barrier, enabling workloads from different customers to run on the same machine, without any trade-offs to security or efficiency | You can create and configurate limiters via the Firecracker API, and define flexible rate limiters that supports verse or specific bandwidth or operations limitations
    Firecracker is an alternative to QEMU, an established VMM with a general purpose and broad feature set that allows it to host a variety of guest operating systems | Firecracker also provides the metadata service that securely shares configuration information between the host and guest operating system. 



### Firecracker VMM integration with Host OS

![](/images/fc_host_integration.png)


### Firecracker Security

![](/images/fc_security.png)


### Firecracker REST API

- The API is accessible through HTTP calls on specific URLs carrying JSON model data.
- The transport medium is a Unix domain socket. 
    ![](/images/fc_api_specification.png)
- 'root' or '/' - returns general information about an instance
- '/actions' 
    - instant start - powers on the uVM and starts the guest OS
    - block device rescan - to trigger a rescan of one of the
uVMs attached block devices
    - flush metrics - flushes the metrics on user demand.
    - send CTRL+ALT+DEL - to shutdown uVM
- GET '/machine-config' - gets the machine configuration of the VM. 
- PUT '/machine-config' - updates the virtual machine configuration with the specified input. 
    - If any of the parameters has an incorrect value, the whole update fails.
- '/drives' - resource creates new drive with ID specified by drive_id parameter. 
    - If a drive with the specified ID already exists it updates the state based on new input. 
    - It will fail if the update is not possible.

## Firecracker and AWS Lambda

- AWS Lambda functions execute in a container (sandbox) that isolates them from other functions and provides the resources, such as memory, specified in the function’s configuration. 
- It's no secret that multiple lambda functions run on the same hardware and on the same host at the same time 
    - Reason: It's just not cost-effective for AWS to buy data center servers with 128MB of RAM 
- One of the most frequent questions here is: `How do we isolate different functions that are running on a particular lambda worker?`
- Isolation - Keeping Workloads safe and separate
    - One of the meanings of isolation is security
    - And the second is operational isolation - To ensure that functions run at a consistent performance when there are other functions on the same hardware (i.e., not to get influenced by the noisy neighbours lambda functions)


### Software stack running on Lambda Workers    

![](/images/lambda_software_stack.png)

- Description of each layer in the Lambda Software Stack:
    1. At the top of the stack is is the most important part and that is your code from the function zip or it comes from the lambda layers.
    2. The next layer down is the lambda runtime - This is the Java or or nodejs or Python that comes built into lambda.
    3. A Sandbox contains a pretty full-featured copy of Linux
    4. Guest OS is Amazon Linux. Multiple guest OS's (1000's maybe) are run on a box isolated from each other using virtualization using a hypervisor. 
    5. Host OS is again Amazon Linux and hypervisor runs on it. 
    6. The hardware itself.
- From an isolation perspective, 
    - The first three layers are only used by one function
        - Multiple invocations will land in the same sandbox in serial they won't overlap concurrently and that's where lambda functions scale up. 
        - A sandbox is never reused across multiple functions.
    - The guest OS's are shared within an account
        - Multiple functions within one account will run on the same guest OS either at the same time or when we destroy the sandbox for one function and recreate one 
        - These guest OS's are shared across functions but never shared across multiple AWS accounts. 
        - The boundary between multiple AWS accounts is virtualization and this is the minimum security bar for isolation of functions between accounts and in a lot of ways also the minimum operational isolation bar. 
    - Multiple AWS accounts can share the same Hardware and the Host OS.

### Deep Dive into achieving operational and security isolation

![](/images/lambda_isolation_deepdive.png)

- Underneath the sandbox layer is the same technology that powers linux containers.
- Linux containers don't really exist. Instead containers are kind of grouping of different functionalities which are built in and provided by the linux kernel.
- A number of tools are used from this toolbox provided by the linux kernel. They are:
    - *C groups:* `limits how much you can use`
        - Control Groups provide a mechanism for aggregating/partitioning sets of tasks, and all their future children, into hierarchical groups with specialized behaviour.
        - Kernel feature that limits, accounts for, and isolates the resource usage (CPU, memory, disk I/O, network, etc.) of a collection of processes.
        - A mechanism to say this process and obviously anything that forks or any threads that creates is only allowed to use a certain amount of CPU, a certain amount of memory, a certain amount of disk throughput, a certain amount of memory throughput.
        - This is the kind of operational isolation.
    - *Namespaces:* `limits what you can see (and therefore use)`
        - Wraps a global system resource in an abstraction that makes it appear to the processes within the namespace that they have their own isolated instance of the global resource.
        - Namespaces partitions kernel resources such a way that one set of processes sees one set of resources while another set of processes sees a different set of resources.
        -  For instance if you go digging around inside the lambda sandbox you'll see that your lambda function always runs as PID=1.
        - Well, how can you have multiple functions with the same PID number?
        - It's just PID=1 in it's process namespace and it's got a real PID that is NOT 1, but within the namespace (i.e., sandbox), the PID is 1 (Also for user IDs, group IDs, net, mnt, uts, ipc ).
    - *Seccomp:* `kind of Firewall for the kernel`
        - Short for secure computing mode
        - System call is the programmatic way in which a computer program requests a service from the kernel of the operating system it is executed on.
        - Linux kernel has a lot of syscalls to expose the functionality kernel can do like opening sockets, opening files, reading and writing from files. 
        - Seccomp in a simpler way:
            - this process can only call these syscalls
            - cannot call those syscalls
            - can call these syscalls but with only these arguments 
            - can call those syscalls but not with those arguments
        - seccomp bpf reduces the kernel surface area and is restricted to only the functionality that lambda functions actually need to run, making it one of the primary security controls. 
    - *IPtables:* `To provide network isolation`
        - Firewall provides a whole variety of tools necessary for safe and secure exchange of information between hosts in the network. 
        - ip tables, eb tables and routing are few of them.
    - *chroot:* `Simulate a directory on your filesystem as the root of the filesystem`
        - bind mounts and loopback mounts provide the underlying filesystem.
        - bind mount - A bind mount is an alternate view of a directory tree. Classically, mounting creates a view of a storage device as a directory tree. A bind mount instead takes an existing directory tree and replicates it under a different point. The directories and files in the bind mount are the same as the original. Any modification on one side is immediately reflected on the other side, since the two views show the same data.
        - loopback mount - Some platforms provide the capability to mount a file as if it were a block device (like a disk partition. This allows mounting a file as if it were a hard disk, CD-ROM, or any other physical media. The primary advantage to this is that it's a simple way to create or work with a floppy, Zip, or CD-ROM image without needing the physical device. You can mount a CD image without having to burn an actual CD or manipulate a floppy boot image. Of course, different platforms call it different things and use different tools.
- All these mechanisms are sticky. For instance, if you a process to a C group, it can't get out of that. It can't take itself out. 


### Virtualization and Firecracker VMM in AWS Lambda

- Virtualization and Device Emulation is done using virtualization features built into the hardware like VTx on Intel, to make the hardware essentially just pretend to be multiple CPUs instead of one and this is all this all controlled by the hypervisor and virtual machine monitor.
- There are two ways lambda is built today
    - EC2 Instances - Every lambda worker is an EC2 instance here. One instance per account. 
    - Reasons mentioned below:
        - A great security boundary
        - A fast way to build the system
    - Firecracker - On one bare-metal EC2 instance, firecracker VMM launches hundreds of thousands of uVMs. 
        - These are more flexible and agile. 
        - Simplifies the security model.
        - Instead of having the layers of `one function, one account, many accounts` firecracker simplifies this down to `one function in a macro VM and in multiple macro vm's across multiple accounts on a piece of hardware` (More Utilization).
        - It's also nice for the lambda programming model because this provides strong isolation even between functions.
        ![](/images/lambda_firecracker_isolation.png)


### Raise the security bar by innovating the isolation on a bare metal server

- The racks in the data center obviously doesn't have hundreds of thousands of network cards, hard drives or so on. 
- But each guest uVM can see these resources. To user space code running in that uVM those looked like hardware devices.
- This works through the magic of virtualization and a little bit of cooperation between the guest OS kernel and the hypervisor and the implementation of device emulation inside firecracker.
- The implementation of device emulation is done using the virtIO protocol. 
    ![](/images/virtio_ibm.png)
    - VirtIO is an abstraction layer over devices in a paravirtualized hypervisor.
    - VirtIO has drivers inside the guest kernel to implement a block device and a network card in a way that is very simple, secure and efficient. 
    - Efficiency here is achieved by reducing the number of times that the guest OS has to switch between the guest and the host OS.
    - The simplest possible interface is a way for the guest OS to write a character or a block into the host and it would have to do this multiple times to send a network packet, for example. 
    - Instead, VirtIO builds apps and data structures in memory copies these network packets into these data structures calls respective hypervisor functions. 
    - The device emulation implementation picks that stuff up and sends those to the real hardware.
    ![](/images/fc_device_emulation.png)
    - `The innovation in firecracker is that this device emulation runs inside a very restricted sandbox.`
    - So this is a kind of a second layer sandbox it just sits around that device simulation code with very few privileges (This is secured using chroot, seccomp etc discussed above).

## Firecracker and AWS Fargate



## Key Sources: 

- [Reinvent 2018 - Anthony Liguori](https://www.youtube.com/watch?v=DrrvPqX_Qr8)
- [Reinvent 2018 - A Serverless Journey (SRV 409)](https://www.youtube.com/watch?v=QdzV04T_kec)
- [AWS Builders Day](https://www.youtube.com/watch?v=YYgbcb1VM8c)
- [Firecracker Design](https://github.com/firecracker-microvm/firecracker/blob/master/docs/design.md)
- [Firecracker Website](https://firecracker-microvm.github.io/)
- [Jeff Barr Blog](https://aws.amazon.com/blogs/aws/firecracker-lightweight-virtualization-for-serverless-computing/)
- [AWS Open Source Blog on Firecracker](https://aws.amazon.com/blogs/opensource/firecracker-open-source-secure-fast-microvm-serverless/)
- [Katacoda Firecracker learning scenario](https://www.katacoda.com/firecracker-microvm/scenarios/getting-started)
- [Course on LinkedIN](https://www.linkedin.com/learning/firecracker-first-look/what-is-firecracker)
- [Bind Mount](https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount)
- [Bind Mount vs Symbolic Links](https://www.quora.com/What-are-the-differences-between-bind-mounts-and-symlink-on-Unix)
- [Loopback Mount](https://docstore.mik.ua/orelly/unix3/upt/ch44_07.htm)
- [VirtIO](https://developer.ibm.com/articles/l-virtio/)




<!-- - [Discussion about KVM-clock](https://access.redhat.com/discussions/686523)
    - kvm-clock is a Linux clocksource like tsc and hpet.  (tsc is commonly used on physical machines).  The discussion at the [link](http://thread.gmane.org/gmane.linux.kernel/1062438) indicates that all clocksources act like time counters that are periodically read by interrupts.
    - kvm-clock apparently has special functionality to handle the fact that the timing of interrupts is not always precisely regular in VMs. 
    - So, kvm-clock is a time counter and not a virtual hardware clock.  It was not designed to keep the guest's system time in sync with the host's system time.  NTP is required on both the host and the guest for adjusting their system times.
    - The RTC (a.k.a. the hardware clock) is not a Linux clocksource.  It keeps the date and time of day but is not that accurate.  It is not used to update the system time except for when the OS starts up. -->











