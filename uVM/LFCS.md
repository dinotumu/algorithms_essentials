# Practise: Linux Administration
Roadmap to prepare for LFCS exam in 2 weeks. Mostly learning concepts and scripting.

Resources to learn concepts (first 10 weeks):
- Books
    - LFS 300: Fundamentals of Linux
    - LFS 301: Linux System Administration
    - LFS 311: Advanced Linux System Administration and Networking
- LFCS video Lectures
    - Part 1: Essential Commands [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656163&sessionid=1&key=F97C8C0013738513CBB12C0A290695AF&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
    - Part 2: Operation of Running Systems [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656196&sessionid=1&key=EB599B1B67D5674107FD8E2CECB0C098&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
    - Part 3: User and Group Management [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656241&sessionid=1&key=A51CF7ED625023529A168CC2E47A6B3F&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
    - Part 4: Networking [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656149&sessionid=1&key=DF959A16B69DBABBBC6AB7B148072079&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
    - Part 5: Service Configuration [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656260&sessionid=1&key=B568F6D247E4AB16E5A29562409DFEFC&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
    - Part 6: Storage Management / Azure Storage [link](https://event.on24.com/interface/registration/autoreg/index.html?eventid=1656076&sessionid=1&key=44DF506DF21A1CAA048B402719E82472&firstname=Sai+Dinesh&lastname=Tumu&company=NCIRL&email=saidinesh.tumu@gmail.com&job_title=Executive&country=&zip=&work_phone=)
- Scripting Resources
    - [Shell Scripting Tutorial](https://www.shellscript.sh/first.html)
    - [Explain Shell](https://explainshell.com/)
    - [Simplified man pages](https://tldr.sh/)
    - [interviewbit](https://www.interviewbit.com/courses/shell/)

#### Schedule for preparation
- Week 01: learn all concepts
- Week 02: revision + scripting + exam
