## Virtualization

- KVM is a virtual machine capability that's built into the kernel 

- VMM is the thing that essentially runs kvm and sets it up so that it can actually run

- So there's actually some communication between a user mode component which is the VMM and the kernel component which is KVM




- Typically when folks say hey I'm running a KVM hypervisor it's actually QEMU which is driving KVM and so QEMU can both do emulation with no hardware or kernel support but it also can be used to drive the k vm directly and so




## Containers

what you'll find is that a lot of times




people when they talk about hypervisors




they'll use k vm and QEMU or you know QT




k vm driven by QEMU almost




interchangeably













## Table of Contents

- 00:00:00 - Welcome to TGIK!
- 00:04:00 - Week in Review
- 00:14:00 - Let's get Started
- 00:18:00 - Diagram time, how's it work?
- 00:39:00 - Let's do some stuff, but first a roll call
- 00:43:00 - Let's run it, in GCE(!)
- 00:47:00 - Let's check /dev/kvm
- 

## Show Notes

What is Firecracker?

Micro VM - virtual machine manager 
- [Announcement Blog](https://aws.amazon.com/blogs/aws/firecracker-lightweight-virtualization-for-serverless-computing/)
- [Firecracker Homepage](https://firecracker-microvm.github.io/)
- [Github Repo](https://github.com/firecracker-microvm/firecracker)
- [Announcement Video](https://www.youtube.com/watch?v=femopq3JWJg)
- [Firecracker Katacoda](https://www.katacoda.com/firecracker-microvm/scenarios/getting-started)

Leverages KVM, driven my QEMU, written in Rust. (GCE VMM is C++ back from when Joe worked on it). 
These are the only 4 devices (virtio net / virtio block / 1-pin of i8042, and UART). eg. no GPU support.
Firecracker is ~40k lines of code, including the auto-generated system call bindings. 
Most importantly, the logo has 7 sides, so it's clearly designed for Kubernetes