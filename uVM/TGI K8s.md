Emulation | Simulation | Virtualization
--- | --- | ---



Firecracker builds on top of KVM.

- A lot of times people when they talk about the hypervisor they talk about KVM.
- But actually KVM is a virtual machine capability that's built into the kernel. 
- It doesn't do anything in and of itself it actually needs a user space via memory to drive the capabilities that are there laid in the kernel.
- VMM is the thing that essentially runs KVM and sets it up so that it can actually run in so there's actually a carrying between a user mode component which is via the vmm and the kernel components which are KVM 
- Typically when people say hey I'm running a KVM hypervisor it's actually QEmu which is actually driving KVM. 
- QEmu is a complete and standalone software of its own. It emulates (Emulation refers to the ability of a computer program in an electronic device to emulate (or imitate) another program or device) machines through dynamic binary translation. 
- This is a special 'recompiler' that transforms binary code written for a given processor into another one (i.e., source instruction set to target instruction set). To emulate more than just the processor, Qemu includes a long list of peripheral emulators: disk, network, VGA, PCI, USB, serial/parallel ports, etc.
- 


 








    Technology Stack | How Virtualization is done?
    --- | ---
    QEMU Only | a
    QEMU with KVM | Emulation with KVM
    KVM Only | as









here's your kernel

and here's user mode okay and this is a

machine Linux machine and inside the

kernel there's all sorts of different

devices and stuff but one of these

things is KVM and KVM actually gets

exposed through slash dev slash k vm and

so that's a device like a block device

to some degree I mean it's a character

mode device there's different types but

like a serial there's all so this is how

essentially the the kernel presents

interfaces or API is up into user mode

and then generally what you have is you

have something like Q this will actually

go through and talk to KVM and all the

stuff that interacts with the underlying

hardware so like let's say here we have

the processor and typically this is x86

but KVM can actually work with other

processors also but

essentially takes some of these special

instructions and special capabilities of

the processor to enable hardware

assisted virtualization and it packages

it up in a useful way for the for the OS

to be able to be shared and so you run a

user mode process which is KQ mu and it

goes ahead and it talks to KVM and it

says hey create a virtual machine

context for me KBM goes ahead and does

that and then essentially what happens

is that there's a thread inside of the

VM and each thread corresponds to a

guest processor and and so you know and

so this is also your VM M right and so

now there's a whole bunch of stuff that

like for booting up a machine that is

not supported natively by KVM and so

qemu can actually take care a lot of

that so like it turns out that like with

bios and stuff like that there's a ton

of emulation that QEMU has to do and

that's why there's a lot of shared

between sort of the pure emulation and

the hardware assisted stuff so that's

that's a relationship between the VM m

in k vm and it turns out that this vm m

logically is pretty simple but in

reality because of a lot of the legacy

that exists for being able to boot a

machine QEMU ends up being fairly

heavyweight now the way that this works

is typically is is that each of these

threads in here actually makes a call

into k vm says switch me into guest

context and when it switches into guest

context there's a whole bunch of

information that gets passed through

things like you know what the

instruction pointer and stuff like that

is and then it runs in the guest and

then something interesting happens like

you hit an interrupt or you hit a

protected piece of memory and at that

point what happens is that it pops out a

guest mode into host mode and when it's

in host mode the host then goes ahead

and this is something that the VM M does

it goes through and and deals with that

exceptional thing right and so a lot of

this is dealing with typical devices

like IO

like Network block devices keyboards

that type of thing and so what happens

is that when the the guest says hey I

want to write a packet out to the

network it either through an interrupt

or a protected memory it pops back out

into host mode and then up to the host

mode to actually take that information

depending on how it interfaces for that

device and do whatever it needs to do

and then pop that particular thread back

into guest mode and so the loop in a VM

M is actually fairly fairly minimal in

terms of its like you know run in guest

until you know interesting thing you

know do interesting thing and then you

know continue you know there's

essentially a little bit of a loop that

does there now the the hard part is you

know number one getting to the point

where I can actually run this loop right

because there's a lot of that sort of

like lizard brain stuff that the VM has

to take care of and then the second

thing is that this do interesting thing

here ends up depending on how compatible

you want to be with like you know being

it will take your windows do you know

USB or disk or ISO we mentioned run it

depending on how compatible you want to

be the interesting things can be very

very interesting now one of the things

that one of the things that firecracker
