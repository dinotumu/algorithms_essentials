<!-- ## Contents

1. [Linux Kernel](#linux-kernel)
2. [Linux Filesystem](#linux-filesystem)
3. [Linux Boot Process](#linux-boot-process)
4. [Hypervisors and Virtual Machines]()
5. [Containers]()
6. [Pros and Cons of VMs and Containers]() -->


## Linux Kernel

- The Linux kernel is not an operating system. It enables the applications that make the OS able to operate. Such applications include the various shells, the compiler, the windowing software, utilities and so on that are needed before you computer will even boot up.

- The Linux kernel is a monolithic kernel. This means that the whole operating system is on the RAM reserved as kernel space. The kernel owns that space on the RAM until the system is shutdown. With kernel space, once the RAM space is taken, nothing else can have that space.

- On contrast, user space is the space on the RAM that the user's programs own. Applications like web browsers, video games, word processors, media players, the wallpaper, themes, etc. are all on the user space of the RAM. When an application is closed, any program may use the newly freed space.

- The kernels at [kernel.org](https://www.kernel.org/) are vanilla kernels. Each different distribution takes these vanilla kernels and adds their own type of flavoring. Although all kernels are derived from the same kernel source, not all kernels are equal.
    - They may fix particular bugs in the code (Debian is very good at doing this) 
    - They may add their own patches to do fancy things (SuSE adds a patch to do a boot screen animation)
    - They may add patches to enhance the facilities of the kernel (often using the source from the developmental branch of the kernel eg for very new USB devices)

- So if you have a particular distribution and take advantage of some of the non standard features of the kernel, and then go and download a vanilla kernel, your hot new trendy USB device may no longer work and if you are on SusE you will no longer see a flashy boot time animation.


## Linux Filesystem

-  `In Linux, everything is configured as a file.` 
    - This includes not only text files, images and compiled programs (also referred to as executables), but also directories, partitions (A partition is a section of a hard disk that contains a single type of filesystem.) and hardware device drivers ('/dev').

- The term filesystem has two somewhat different meanings, both of which are commonly used.

- `Linux Root Filesystem` - One meaning is the entire hierarchy of directories (also referred to as the directory tree) that is used to organize files on a computer system. 
    - On Linux and Unix, the directories start with the root directory (designated by a forward slash), which contains a series of subdirectories, each of which, in turn, contains further subdirectories, etc.
    - A variant of this definition is the part of the entire hierarchy of directories or of the directory tree that is located on a single partition or disk.
    - The `root filesystem (rootfs)` is the filesystem that is contained on the same partition on which the root directory is located, and it is the filesystem on which all the other filesystems are mounted (i.e., logically attached to the system) as the system is booted up.
        - Rather, one of the important kernel boot parameters is "root=", which tells the kernel where to find the root filesystem. For instance, `root=/dev/hda1` tells that root file system is located in hda1.
    - Filesystem contains a control block, which holds information about that filesystem. The other blocks in the filesystem are inodes, which contain information about individual files, and data blocks, which contain the information stored in the individual files.
    - There is a substantial difference between the way the user sees the Linux filesystem and the way the kernel actually stores the files. 
        - `To the user` the filesystem appears as a hierarchical arrangement of directories that contain files and other directories (i.e., subdirectories). 
            - Directories and files are identified by their names. This hierarchy starts from a single directory called root, which is represented by a "/".
            - The Filesystem Hierarchy Standard (FHS) defines the main directories and their contents in Linux and other Unix-like operating systems. 
            - All files and directories appear under the root directory, even if they are stored on different physical devices (e.g., on different disks or on different computers). 
            - A few of the directories defined by the FHS are /bin (command binaries for all users), /boot (boot loader files such as the kernel), /home (users home directories), /mnt (for mounting a CDROM or floppy disk), /root (home directory for the root user), /sbin (executables used only by the root user) and /usr (where most application programs get installed).
            - The exact contents of the root filesystem will vary according to the computer and/or the linux distribution, but they will include the files that are necessary for booting the system and for bringing it up to such a state that the other filesystems can be mounted as well as tools for fixing a broken system and for recovering lost files from backups. 
        - `To the Linux kernel` the filesystem is flat. 
            - That is, it does not 
                1. have a hierarchical structure, 
                2. differentiate between directories, files or programs 
                3. identify files by names. 
            - Instead, the kernel uses inodes to represent each file. An inode is actually an entry in a list of inodes referred to as the inode list. 
            - Each inode contains information about a file including 
                1. its inode number (a unique identification number) 
                2. the owner and group associated with the file
                3. the file type (for example, whether it is a regular file or a directory)
                4. the file's permission list 
                5. the file creation, access and modification times 
                6. the size of the file 
                7. the disk address (i.e., the location on the disk where the file is physically stored)
            - The inode numbers for the contents of a directory can be seen by using the `ls -i` command.
            - The `df -m` command is used to show information about each of the filesystems which are currently mounted on (i.e., connected to) a system, including their allocated maximum size, the amount of disk space they are using, the percentage of their disk space they are using and where they are mounted (i.e., the mountpoint). 
            - A column showing the type of each of these filesystems can be added to the filesystem table produced by the above command by using the `df -m --print-type`
- `Linux Native Filesystems` - The second meaning is the type of filesystem, that is, how the storage of data (i.e., files, folders, etc.) is organized on a computer disk (hard disk, USB drive, CDROM, etc.) or on a partition on a hard disk. 
    - Each type of filesystem has its own set of rules for controlling the allocation of disk space to files and for associating data about each file (referred to as meta data) with that file, such as its filename, the directory in which it is located, its permissions and its creation date. 
    - Every native Linux filesystem implements a basic set of common concepts that were derived from those originally developed for Unix.
    - Several Linux native filesystems are currently in widespread use, including ext4, ext3, btrfs, ReiserFS, JFS and XFS. 
    - A filesystem can be mounted anywhere in the directory tree; it does not necessarily need to be mounted on the root filesystem. For example, it is possible (and very common) to have one filesystem mounted at a mount point on the root filesystem, and another filesystem mounted at a mount point contained in that filesystem. 
    - Journaling filesystems offer several important advantages over static filesystems, such as ext4. In particular, if the system is halted without a proper shutdown, they guarantee consistency of the data and eliminate the need for a long and complex filesystem check during rebooting. The term journaling derives its name from the fact that a special file called a journal is used to keep track of the data that has been written to the hard disk. 
    - Unlike most other operating systems, Linux supports a large number of foreign filesystems in addition to its native filesystems. This is possible because of the virtual file system layer, which was incorporated into Linux from its infancy and makes it easy to mount other filesystems. In addition to reading, foreign filesystem support also often includes writing, copying, erasing and other operations.


- The other filesystems are tmpfs, initramfs, pseudo fs etc.. 





[asd](https://www.quora.com/What-is-Linux-Kernel-Can-it-be-installed-run-on-a-sytem-without-any-GNU-software)

[asd](https://www.linuxquestions.org/questions/linux-general-1/what-is-vanilla-kernel-79388/)

[](http://www.linfo.org/filesystem.html)


[](http://www.linfo.org/root_filesystem.html)

[](https://www.mjmwired.net/kernel/Documentation/x86/boot.txt)

[](https://opensource.com/article/17/2/linux-boot-and-startup)