## AWS Firecracker demo on Local machine
 
- Configuration of Local Machine used to run this demo
    - Intel Core i5 4th Generation 4200U 1.7 GHz Dual-Core Processor. Includes 
        - SSE4.1/4.2
        - AVX 2.0
        - VT-x with EPT (Extended Page Tables)
        - VT-d
        - vPro
    - 8GB DDR3 Dual-Channel RAM
    - 1TB Disk Space
- Fedora30 is installed on the local machine directly
    - Firecracker is not well tested for nested virtualization, yet!

## Setup for Production Host Machine (Dummies)  

- Login as root user or switch to root user using the command below

    ```bash
    sudo su -
    ```
- Install KVM and enable it 

- BASH script to check if your system meets the basic requirements to run Firecracker VMM

    ```bash
    err=""; [ "$(uname) $(uname -m)" = "Linux x86_64" ] || err="ERROR: your system is not Linux x86_64."; [ -r /dev/kvm ] && [ -w /dev/kvm ] || err="$err\nERROR: /dev/kvm is innaccessible."; (( $(uname -r | cut -d. -f1)*1000 + $(uname -r | cut -d. -f2) >= 4014 )) || err="$err\nERROR: your kernel version ($(uname -r)) is too old."; dmesg | grep -i "hypervisor detected" && echo "WARNING: you are running in a VM. Firecracker is not well tested under nested virtualization."; [ -z "$err" ] && echo "Your system looks ready for Firecracker!" || echo -e "$err"
    ```
- Create a non-privileged (i.e., non-root) user and group.  
    
    ```bash
    useradd fc_host        # To Create user and group
    passwd fc_host         # Set password for the new user
    ```
- Quick check if everything is okay  

    ```bash
    cat /etc/passwd | grep fc_host
    ``` 
    - The output should be something like this
        
        ```bash 
        fc_host:x:1001:1002::/home/fc_host:/bin/bash
        ```
- Add `fc_host` to the `wheel` group to grant sudo access
    
    ```bash
    usermod -aG wheel fc_host
    ```
- Switch to the `fc_host` user

    ```bash
    sudo su - fc_host
    ```
- Grant `/dev/kvm` read/write permissions to `fc_host`. Since, switched to fc_host now, `${USER}` can be used instead 

    ```bash
    sudo setfacl -m u:${USER}:rw /dev/kvm
    ```
- Firecracker is linked statically against [musl](https://www.musl-libc.org/)
    - i.e., the library code containing external functions etc. is stored within the executable rather than in separate files.
    - Therefore, no depencies or DLL hell. Just download the binary and run.

- Download the latest `firecracker` and `jailer` binaries from [release page](https://github.com/firecracker-microvm/firecracker/releases)

    ```bash
    curl -LOJ https://github.com/firecracker-microvm/firecracker/releases/download/v0.16.0/firecracker
    curl -LOJ https://github.com/firecracker-microvm/firecracker/releases/download/v0.16.0/jailer
    ```
- Move the binaries `firecracker` and `jailer` to `/usr/local/bin`

    ```bash
    sudo mv firecracker /usr/bin/firecracker
    sudo mv jailer /usr/bin/jailer
    ```
- Firecracker uses the jailer binary to create the secure, execution jail for the instance.

- Make the binaries executable

    ```bash
    chmod +x /usr/bin/firecracker /usr/bin/jailer
    ```
- Check to ensure that `firecracker` and `jailer` binaries can run by outputting the version number

    ```bash
    firecracker -V
    jailer -V
    ```

<!-- - Clone and build `firectl`, a command-line tool to run Firecracker microVMss

    ```bash
    sudo dnf install -y git
    git clone https://github.com/firecracker-microvm/firectl
    sudo amazon-linux-extras install -y golang1.11
    cd firectl
    make
    ``` -->

## Running Firecracker with jailer

sudo /usr/bin/jailer --id 151e7604-e35c-42b3-b825-416853441234 --node 0 --exec-file /usr/bin/firecracker --uid 1000 --gid 1000


## Running Firecracker uVM

- On Terminal 1

    ```bash
    /usr/bin/jailer --id 551e7604-e35c-42b3-b825-416853441234 --node 0 --exec-file /usr/bin/firecracker --uid 1000 --gid 1000 
    ```



/srv/jailer/firecracker/551e7604-e35c-42b3-b825-416853441234/root/api.socket

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/boot-source'   \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{
        "kernel_image_path": "./hello-vmlinux.bin",
        "boot_args": "console=ttyS0 reboot=k panic=1 pci=off"
    }'

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/drives/rootfs' \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{
        "drive_id": "rootfs",
        "path_on_host": "./hello-rootfs.ext4",
        "is_root_device": true,
        "is_read_only": false
    }'

curl -X PUT \
  --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
  http://localhost/network-interfaces/wlp2s0 \
  -H accept:application/json \
  -H content-type:application/json \
  -d '{
      "iface_id": "wlp2s0",
      "guest_mac": "AA:FC:00:00:00:01",
      "host_dev_name": "tap0"
    }'

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/actions'       \
    -H  'Accept: application/json'          \
    -H  'Content-Type: application/json'    \
    -d '{
        "action_type": "InstanceStart"
     }'







uncompressed Linux kernel binary



ext4 file system image (to use as rootfs). 

You can use these files from our
microVM image S3 bucket:
[kernel](
https://s3.amazonaws.com/spec.ccfc.min/img/hello/kernel/hello-vmlinux.bin
), and
[rootfs](
https://s3.amazonaws.com/spec.ccfc.min/img/hello/fsfiles/hello-rootfs.ext4
).



Now, let's open up two shell prompts: one to run Firecracker, and another one
to control it (by writing to the API socket). For the purpose of this guide,
**make sure the two shells run in the same directory where you placed the
`firecracker` binary**.

In your **first shell**:

- make sure Firecracker can create its API socket:

```bash
rm -f /tmp/firecracker.socket
```

- then, start Firecracker:

```bash
./firecracker --api-sock /tmp/firecracker.socket
```

In your **second shell** prompt:

- get the kernel and rootfs, if you don't have any available:

  ```bash
  curl -fsSL -o hello-vmlinux.bin https://s3.amazonaws.com/spec.ccfc.min/img/hello/kernel/hello-vmlinux.bin
  curl -fsSL -o hello-rootfs.ext4 https://s3.amazonaws.com/spec.ccfc.min/img/hello/fsfiles/hello-rootfs.ext4
  ```

- set the guest kernel:

  ```bash
  curl --unix-socket /tmp/firecracker.socket -i \
      -X PUT 'http://localhost/boot-source'   \
      -H 'Accept: application/json'           \
      -H 'Content-Type: application/json'     \
      -d '{
          "kernel_image_path": "./hello-vmlinux.bin",
          "boot_args": "console=ttyS0 reboot=k panic=1 pci=off"
      }'
  ```

- set the guest rootfs:

  ```bash
  curl --unix-socket /tmp/firecracker.socket -i \
      -X PUT 'http://localhost/drives/rootfs' \
      -H 'Accept: application/json'           \
      -H 'Content-Type: application/json'     \
      -d '{
          "drive_id": "rootfs",
          "path_on_host": "./hello-rootfs.ext4",
          "is_root_device": true,
          "is_read_only": false
      }'
  ```

- start the guest machine:

  ```bash
  curl --unix-socket /tmp/firecracker.socket -i \
      -X PUT 'http://localhost/actions'       \
      -H  'Accept: application/json'          \
      -H  'Content-Type: application/json'    \
      -d '{
          "action_type": "InstanceStart"
       }'
  ```

Going back to your first shell, you should now see a serial TTY prompting you
to log into the guest machine. If you used our `hello-rootfs.ext4` image,
you can login as `root`, using the password `root`.

When you're done,
issuing a `reboot` command inside the guest will shutdown Firecracker
gracefully. This is because, since microVMs are not designed to be restarted,
and Firecracker doesn't currently implement guest power management, we're
using the keyboard reset action as a shut down switch.

**Note**: the default microVM will have 1 vCPU and 128 MiB RAM. If you wish to
customize that (say, 2 vCPUs and 1024MiB RAM), you can do so before issuing
the `InstanceStart` call, via this API command:

```bash
curl --unix-socket /tmp/firecracker.socket -i  \
    -X PUT 'http://localhost/machine-config' \
    -H 'Accept: application/json'            \
    -H 'Content-Type: application/json'      \
    -d '{
        "vcpu_count": 2,
        "mem_size_mib": 1024
    }'
```














