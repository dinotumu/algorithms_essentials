#!bin/bash -e

sudo -i

ID=151e7604-e35c-42b3-b825-416853441234

cd /srv/jailer/firecracker/551e7604-e35c-42b3-b825-416853441234/root/

curl -fsSL -o hello-vmlinux.bin https://s3.amazonaws.com/spec.ccfc.min/img/hello/kernel/hello-vmlinux.bin
curl -fsSL -o hello-rootfs.ext4 https://s3.amazonaws.com/spec.ccfc.min/img/hello/fsfiles/hello-rootfs.ext4

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/boot-source'   \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{
        "kernel_image_path": "./hello-vmlinux.bin",
        "boot_args": "console=ttyS0 reboot=k panic=1 pci=off"
    }'

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/drives/rootfs' \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{
        "drive_id": "rootfs",
        "path_on_host": "./hello-rootfs.ext4",
        "is_root_device": true,
        "is_read_only": false
    }'

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/network-interfaces/wlp2s0' \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{
        "iface_id": "wlp2s0",
        "guest_mac": "AA:FC:00:00:00:01",
        "host_dev_name": "tap0"
    }'

curl --unix-socket /srv/jailer/firecracker/151e7604-e35c-42b3-b825-416853441234/root/api.socket -i \
    -X PUT 'http://localhost/actions'       \
    -H  'Accept: application/json'          \
    -H  'Content-Type: application/json'    \
    -d '{
        "action_type": "InstanceStart"
    }'