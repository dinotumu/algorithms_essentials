# Linux Boot Process in x86 based systems

- When you press the power button, the computer supplies power to its components—the motherboard, CPU, hard disks, solid state drives, graphics processors, and everything else in the computer.

- Now that it has electricity, the CPU initializes itself and looks for a small program that is typically stored in a chip on the motherboard. This computer program is also known as firmware. Firmware provides the low-level control for the device’s specific hardware. It is stored in non-volatile memory like ROM, EPROM, or flash memory on the motherboard.

- For x86 based architectures, the firmware can be legacy BIOS (Basic Input/Output System) or UEFI (Unified Extensible Firmware Interface) or coreboot. Coreboot is a popular open source firmware. Only UEFI and BIOS will be discussed here. 

- The CPU runs the UEFI or BIOS, which tests and initializes your system’s hardware—including the CPU itself. For example, if your computer doesn’t have any RAM, it will beep and show you an error, stopping the boot process. This is known as the POST (Power On Self Test) process.

- The POST tests are not much useful as a Hardware diagnostic test. It just test the system hardware components, and loads a boot loader. The main two errors occur during POST are:
    - Fatal error. This occurs due to hardware problems.
    - Non fatal error. Due to software problems.

- The operating system is loaded into memory by a special program called a boot loader. A boot loader usually exists on the system's primary hard drive (or other media device) and has the sole responsibility of loading the Linux kernel with its required files or (in some cases) other operating systems into memory. 

- The bootloader is accessed in different way in BIOS and UEFI based systems. The bootloader discussed here is GRUB2 (GRand Unified Boot loader). GRUB is the program which makes the computer just smart enough to find the operating system kernel and load it into memory. 

## BIOS-MBR based x86 systems

- At the end of POST, BIOS searches the attached disks for a boot record, usually located in the Master Boot Record (MBR). The MBR is the most important data structure on the disk and is created when the disk is partitioned. The location of MBR is always track (cylinder) 0, side (head) 0, and sector 1. This is the `Stage 1 or primary boot loader`. It consists of 
    - a 446 byte `boot.img` file (boot.img is a part of the grub package and is copied (written) to the MBR while installing GRUB)
    - a 64 byte data structure holding the information about primary partitions and an extended partition (Each partition table entry is 16 byte long, making a maximum of four entries available)
    - a 2 byte signature (which is always set to 0x55AA indicating the end of MBR)

        ![](/images/basics_mbr.png)
- To see the contents of MBR, use these commands:
    ```bash
    dd if=/dev/hda of=mbr.bin bs=512 count=1
    od -xa mbr.bin
    ```
    - The dd command, which needs to be run from root, reads the first 512 bytes from /dev/hda and writes them to the mbr.bin file. The od command prints the binary file in hex and ASCII formats.
    - The output of the aboove commands is provided below:
        ```bash
        00000000: 00 33 C0 8E D0 BC 00 7C  - 8B F4 50 07 50 1F FB FC   .3.....|..P.P..
        00000010: BF 00 06 B9 00 01 F2 A5  - EA 1D 06 00 00 BE BE 07   ................
        00000020: B3 04 80 3C 80 74 0E 80  - 3C 00 75 1C 83 C6 10 FE   ...<.t..<.u.....
        00000030: CB 75 EF CD 18 8B 14 8B  - 4C 02 8B EE 83 C6 10 FE   .u......L.......
        00000040: CB 74 1A 80 3C 00 74 F4  - BE 8B 06 AC 3C 00 74 0B   .t..<.t.....<.t.
        00000050: 56 BB 07 00 B4 0E CD 10  - 5E EB F0 EB FE BF 05 00   V.......^.......
        00000060: BB 00 7C B8 01 02 57 CD  - 13 5F 73 0C 33 C0 CD 13   ..|...W.._s.3...
        00000070: 4F 75 ED BE A3 06 EB D3  - BE C2 06 BF FE 7D 81 3D   Ou...........}.=
        00000080: 55 AA 75 C7 8B F5 EA 00  - 7C 00 00 49 6E 76 61 6C   U.u.....|..Inval
        00000090: 69 64 20 70 61 72 74 69  - 74 69 6F 6E 20 74 61 62   id partition table
        000000A0: 6C 65 00 45 72 72 6F 72  - 20 6C 6F 61 64 69 6E 67   le.Error loading
        000000B0: 20 6F 70 65 72 61 74 69  - 6E 67 20 73 79 73 74 65    operating system
        000000C0: 6D 00 4D 69 73 73 69 6E  - 67 20 6F 70 65 72 61 74   m.Missing operation
        000000D0: 69 6E 67 20 73 79 73 74  - 65 6D 00 00 80 45 14 15   ing system...E..
        000000E0: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        000000F0: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000100: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000110: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000120: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000130: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000140: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000150: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000160: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000170: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000180: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        00000190: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        000001A0: 00 00 00 00 00 00 00 00  - 00 00 00 00 00 00 00 00   ................
        000001B0: 00 00 00 00 00 00 00 00  - FD 4E F2 14 00 00         .........N......
        80 01                 ..
        000001C0: 01 00 06 0F 7F 96 3F 00  - 00 00 51 42 06 00 00 00   .....?...QB....
        000001D0: 41 97 07 0F FF 2C 90 42  - 06 00 A0 3E 06 00 00 00   A....,.B...>....
        000001E0: C1 2D 05 0F FF 92 30 81  - 0C 00 A0 91 01 00 00 00   .-....0.........
        000001F0: C1 93 01 0F FF A6 D0 12  - 0E 00 C0 4E 00 00 55 AA   ...........N..U.
        ```

        <!-- ![](/images/basics_mbr_partition.png) -->

- BIOS cannot read partition tables or file systems. Therefore it loads boot.img file into the memory from MBR in the disk. The sole function of boot.img is to read the first sector of the core image from a local disk and jump to it. 
- Because of the size restriction, boot.img cannot understand any file system structure, so grub-install hardcodes the location of the first sector of the core image into boot.img when installing GRUB. It is capable of loading either the Stage 1.5 or Stage 2 boot loader.


- It addresses diskboot.img by a 64-bit LBA address, thus it can load from above the 2 GiB limit of the MBR. The actual sector number is written by grub-install

- The `Stage 1.5 boot loader` is read into memory by the Stage 1 boot loader, if necessary. Some hardware requires an intermediate step to get to the Stage 2 boot loader. 
- This is sometimes true when the /boot/ partition is above the 1024 cylinder head of the hard drive or when using LBA mode. The Stage 1.5 boot loader is found either on the /boot/ partition or on a small part of the MBR and the /boot/ partition.







        - The `Stage 2 or secondary boot loader` is read into memory. The secondary boot loader displays the GRUB menu and command environment. This interface allows the user to select which kernel or operating system to boot, pass arguments to the kernel, or look at system parameters.
        - The secondary boot loader reads the operating system or kernel as well as the contents of /boot/sysroot/ into memory. Once GRUB determines which operating system or kernel to start, it loads it into memory and transfers control of the machine to that operating system. 

    - The method used to boot Linux is called direct loading because the boot loader loads the operating system directly. There is no intermediary between the boot loader and the kernel.
    - The boot process used by other operating systems may differ. For example, the Microsoft Windows operating system, as well as other operating systems, are loaded using chain loading. Under this method, the MBR points to the first sector of the partition holding the operating system, where it finds the files necessary to actually boot that operating system.
    - GRUB supports both direct and chain loading boot methods, allowing it to boot almost any operating system. 



            ```bash
            Filesystem     Type     1M-blocks   Used Available Use% Mounted on
            /dev/sda5      ext4           976    160       749  18% /boot
            ```



## UEFI-based x86 systems
- GRUB loads itself into memory in the following stages:
    - The UEFI-based platform reads the partition table on the system storage and mounts the EFI System Partition (ESP), a VFAT partition labeled with a particular globally unique identifier (GUID). The ESP contains EFI applications such as bootloaders and utility software, stored in directories specific to software vendors. Viewed from within the Red Hat Enterprise Linux 6.9 file system, the ESP is /boot/efi/, and EFI software provided by Red Hat is stored in /boot/efi/EFI/redhat/.
    - The /boot/efi/EFI/redhat/ directory contains grub.efi, a version of GRUB compiled for the EFI firmware architecture as an EFI application. In the simplest case, the EFI boot manager selects grub.efi as the default bootloader and reads it into memory.
    - If the ESP contains other EFI applications, the EFI boot manager might prompt you to select an application to run, rather than load grub.efi automatically.
    - GRUB determines which operating system or kernel to start, loads it into memory, and transfers control of the machine to that operating system. 
- Because each vendor maintains its own directory of applications in the ESP, chain loading is not normally necessary on UEFI-based systems. The EFI boot manager can load any of the operating system bootloaders that are present in the ESP. 

    ```bash
    Filesystem     Type     1M-blocks   Used Available Use% Mounted on
    /dev/sda2      vfat            96     46        51  48% /boot/efi
    ```





- Kernel initialization
- Start systemd, the parent of all processes






# U-boot based ARM systems






