# Programming/Coding approach

- Think out loud.
- As much as 70% of the time should be spent in steps 1-3.

### 1. Understand and Analyse the Problem
- Read the problem completely twice and ask questions to seek clarity over ambiguity
- The primary goal is achieved only when you have nswers to these questions
    - Are you able to understand the question completely?
    - Would you be able to explain the question to a layman?
    - What and how many inputs are required?
    - What would be the output for those inputs?
    - Can you separate out some modules or parts from the problem?
    - Do you have enough information? If not, try understanding the question again.

### 2. Go through sample inputs and solve the problem manually
- Given input X, what are the steps necessary to return output Y?
- Try to solve problem using atleast 3-5 sample inputs
- Look at the I/O sample, imagine few corner cases and edge cases 
    - Corner case: a problem or situation that occurs outside of normal operating parameters, specifically when multiple environmental variables or conditions are simultaneously at extreme levels, even though each parameter is within the specified range for that parameter.
    - Edge case: problem or situation that occurs only at an extreme (maximum or minimum) operating parameter
        - Try out the problem with no input, what should be the output now
        - Try out the problem with invalid input, what should be the output now
- If you fall into the pit of false understanding, coming out of it gets really difficult, especially with boundary cases. 
- Sometimes even the reverse can happen, where you look at the boundary cases and interpret the problem as something that is totally different from what it actually is.
- Many people say “understand the problem”. It’s really easy to fool yourself. You don’t understand a problem until you have a complete set of test cases.

### 3. Flowchart
- Break down the problem. Apply diverge/converge thinking process
- Try to make a flow chart or a UML for the problem in hand
    - Divide the problem into different modules or sub-problems
    - Try to make independent functions for each sub-problem
    - Connect those sub-problems by calling them in the required order, or as necessary (Probably one function would be calling another)
    - (Optional) Try to use classes and objects while handling questions which try to implement some real-world problem (like management systems, etc.)
- Also, when you are giving some interview, do not waste time in figuring out the whole solution and then tell your interviewer, keep simplifying the problem and keep telling your interviewer about how you are approaching the problem
    - Tell the interviewer how you are trying to start
    - Tell what method is there in your mind right now
    - Find out the most difficult part you are facing in that problem
    - Ignore that “most difficult” part for some time and start solving a simpler sub-part, that’ll buy you more time to think about the former part
    - Once done with the simple sub-parts, try incorporating a similar approach for the difficult part as well
    - You might come up with a better solution while doing the problem, tell that to your interviewer

### 4. Write function definition with input, output and parameters
- What is the goal of this function? 
- What am I returning at the end of this function?

### 5. Code. Check Time complexity. Dry run.
- The path to destination is already figured out in the flowchart. Keep these 3 things in mind to not to get sidetracked while writing code. 
    - The point where you started.
    - Where are you right now?
    - What is your destination?
- Don’t write one line of code until you have a test case to test it with. Make use of sample inputs/outputs from step 2.
- Check time complexity of the code and dry run it (sample/edge/corner cases from step 2) while the interviewer reads your code.

### 6. Write unit tests. Debug failed test cases
- Here are some questions you should ask yourself once you are done writing the code
    - Does this code run for every possible input (including the edge/corner cases)
    - Is there any other way to code the desired solution?
    - Is the code efficient? Can it be more efficient?
    - Is the code readable?
    - If someone else showed you this code, would you be able to understand it?

### 7. Agile way of doing things
- Another big mistake is trying to over solve the solution on the first iteration. Keep it simple, don’t try to get fancy.
- Don’t try to write optimal code the first round. 
    - Optimal, incorrect code is worthless; a waste of time; and almost impossible to debug.
    - Obfuscated code is the sign of an immature coder. Compilers know more about optimization that you do: obfuscated code subverts that.
- Ask yourself these questions
    - What are your goals for simplifying and optimizing?
    - Are there any more extra steps you can take out?
    - Are there any variables or functions you ended up not even needing or using?
    - Are you repeating some steps a lot? See if you can define in another function.
    - Can the performance (time complexity) be improved?
        - Optimal time complexity should be O(n) or O(log n)
    - Can some other algorithm be used which provides better results?
    - Are there better ways to handle edge/corner cases?