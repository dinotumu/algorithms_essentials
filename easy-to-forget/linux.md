 - '$ top' updates every 2 seconds
 - '$ jobs' command to list the status of all running jobs in the current terminal window - also 'bg', '\<ctrl+Z>' and 'fg' commands
 - '$ at' to start a job in future [link](https://www.computerhope.com/unix/uat.htm)
 - '$ help \<command>' is a built-in usage of the command, and not all commands implement it, or at least not the same way, however, man is a command by itself which is a pager program that reads manual
 - '/etc/' provides system wide configuration while the config files in $HOME allow users to change/override the system wide configuration




- To connect to the wifi network
    - $ nmcli dev wifi connect dino password dino1234
    - $ nmcli connection show --active
    - $ nmcli con down id connectionname
- $ awk '/em1/ {i++; rx[i]=$2; tx[i]=$10}; END{print rx[2]-rx[1] " " tx[2]-tx[1]}'  <(cat /proc/net/dev; sleep 1; cat /proc/net/dev)


- Only the root filesystem is available when a system is brought up in single user mode. Single user mode is a way of booting a damaged system that has very limited capabilities so that repairs can be made to it. After repairs have been completed, the other filesystems that are located on different partitions or on different media can then be mounted on (i.e., attached to) the root filesystem in order to restore full system functionality. The directories on which they are mounted are called mount points. 

- The root filesystem should generally be small, because it contains critical files and a small, infrequently modified filesystem has a better chance of not becoming corrupted. A corrupted root filesystem will generally mean that the system becomes unbootable (i.e., unstartable) from the HDD, and must be booted by special means (e.g., from a boot floppy). 



- systemctl runs with PID as well 
    - It either shows the service running the process or just shows its in the user session scope. 
    - for user session scope, `~/.config/autostart/` and delete the file causing trouble


- To change user, always start with sudo 
    ```bash
    sudo su - # To switch to root user
    sudo su - fc_host # To switch to user 'fc_host' 
    ```

