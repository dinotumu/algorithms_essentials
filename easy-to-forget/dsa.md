## Algorithm
- The complete set of steps taken together that solve a specific problem, forms an algorithm. 

### Algorithm Characteristics
- Algorithms have several characteristics that could be used to describe them. Any given algorithm has an associated complexity.
    - Space Complexity: describes how much memory and storage space the algorithm needs to do its work. 
    - Time complexity: describes how efficient the algorithm is relative to the size of the input it is given to work on. 

### Input and Output
- Algorithms also typically have a defined set of inputs and output. 
    - Input: what does an algorithm accept?
    - Output: for a set of input values it can work on in order to produce a result. 

### Classification
- It's also possible to talk about algorithm classification using a variety of criteria. 
    - serial/parallel
        - Serial: Some algorithms work on their data sets in sequential fashion, which means that they are serial in nature.
        - Parallel: Whereas a parallel algorithm can break up a data set into smaller pieces and then work on each simultaneously.
    - exact/approximate
        - Exact: In this case the produces a known predictable value
        - Approximate: In this case, the alorithm tries to find an answer that might or might not be exact. For example, a facial recognition algorithm might not give the same answer every single time.
    - deterministic/non-deterministic
        - Deterministic: In this case algorithm executes each step with an exact decision,
        - Non-deterministic: The algorithm attempts to produce a solution using successive guesses, which become more accurate over time.

### Common Algrithms
- Search Algorithms
    - Find specific data in a structure
- Sorting Algorithms
    - Take a dataset and apply a sort order to it
- Computational algorithms
    - Given one set of data, calculate another
- Collection Algorithms
    - Work with collections of data

### Understanding Algorithm Performance
- Measure how an algorithm responds to dataset size
- Big-O notation classifies performance as the input size grows
    - e.g., An algorithm that takes eight times longer to operate on a data set that is twice as large is said to be cubic in time. 

## Data Structures
- Used to organize data so it can be processed
- Common Data Structures
    - Arrays
        - Collection of elements identified by index or key
        - Arrays can have multiple dimensions
    - Linked Lists
        - Collection of data elements called nodes
        - Contain reference to the next node in the list
        - Hold whatever data the application needs
        - singly linkedlist, doubly linkedlist, circular linkedlist
    - Stacks and Queues
        - 
    - Trees
        - 
    - Hash Tables
        - 