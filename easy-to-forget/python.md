- In python everything is an object
- strings and bytes are not directly interchangeable
- strings contain unicode, bytes are raw 8-bit values
- Always 'F' capital in False
- Re-declaring of variables is fine in python
- Variables of different types cannot be combined
    - \>>> print("string" + 123) # ERROR
    - \>>> print("string" + str(123)) # string123 is printed
- global and local variables
- conditional statements lets you use "a if <condition> else b"
    ```bash
    st = "x is less than y" if (x<y) else "x is greater than or the same as y"
    ```
- range() is a generator
- variables need not be declared and they are case sensitive
- return type need not be mentioned explicitly
- strings, tuples are immutable; lists are mutable
- python index starts from 0
- 'Any' operator returns true if any of the items is True. Returns False if empty or all are False.
- 'All' operator returns true if all of the items are true else, returns False.




















- It is almost always preferable to use the built-in functions whenever possible, the performance of the overall code is leveraged by using these functions since they're implemented in the Python run time as native code. 
- Python coding style - Python Enhancement Proposal (PEPs). [link](https://www.python.org/dev/peps/pep-0008/s)
    - Developers spend a significant amount of time working on code that has already been written, either by themselves or by someone else. 
    - By adhering to a consistent programming style, you can make your job, as well as the jobs of your colleagues, a lot easier. 
    - Following the PEP guide is not required. You can write your Python code however you want, as long as it has valid syntax.
    - Some main point in the guide are listed here:
        - Import statements all go at the top of the file, and each should have its own line. 
        - Individual lines of code should be limited to 79 characters, including indentation spaces. 
        - For comments and docstrings, limit those to 72 characters. 
        - This makes it easy to have multiple code windows open and to view them side by side, such as when you're using code difference tools to compare files. 
        - Functions and classes should be separated by two blank lines. 
        - Within class definitions, one blank line should separate methods
        - And there shouldn't be any whitespace around function calls, indexes, or keyword arguments.
        ![](/images/python_whitespaces.png)
- Python Truth values
    - In general, any object is considered to be equivalent to Boolean true, unless it's class defines a Bool method that returns false, or has a len method that returns a zero length.  
    - False and None (equivalent to NULL in other languages) evaluate to False.
    - Numeric zero values:
        - The integer value zero
        - The floating-point value 0.0
        - The complex value type of 0j are all false values
    - Decimal (0) and Fractional (0/x) are False
    - Empty Sets/Collections/Sequences - '', "", (), {}, []