# imports


# init variables


# function definitions

def recursive_built(i):

    # print(tertiary_list)

    if i >= 3:
        result = []
        result_list = []
        result_list.append(i)
        result.append(result_list)
        return 1, result

    print("In call f(", i, ") Calling f(", i+1, ")")
    count1, result = recursive_built(i+1)

    print("In call f(", i, ") Calling f(", i+2, ")")
    count2, result_extend = recursive_built(i+2)

    result.extend(result_extend)

    # print(rec_one + rec_two)
    for l in result:
        l.append(i)

    count = count1 + count2 + 1
    return count, result 

def main():
    """ Main entry point of the app """
    count, result = recursive_built(0)
    print(count)
    print(result)

    # Count leaf nodes - simply add in return

    # Count all nodes or all function calls - Add +1 in return

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()