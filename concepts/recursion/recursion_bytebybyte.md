# Byte by Byte CIM recursion

## Table of contents
1. [Introduction](#introduction)
2. [Key Recursive Concepts](#key-concepts)
    <!-- 1. [Sub paragraph](#subparagraph1) -->
3. [Another paragraph](#paragraph2)

---

## Introduction
- I found that many people basically teach tyou to do a lot of practise and magically you will get good at interviews. Well, that works for some people but doesnot really work for many other people. 

- we help software engineers find their dream jobs by taching them systems to interview efficiently. 

- Go through the modules in order. It is designed so it is a bit easier and progresses to get a bit harder. And there is a reason for that I wanted you to develop all the foudations so you can build on those as we continue further along. 

- Each module teaches the core pattern and a bunch of exmples. Understand the pattern and how this pattern is applied o these different examples. 

- This course is not really about the rproblems themselves, but about the patterns and the strategy for applying patterns.

- So, I want you to keep that in mind as you go through each module.

- If it is feeling too easy you can skip ahead but I dont recommend it. It is one of those things where it might be a little bit easy but probably about the things that you might have not thought  or different ways f thinking about the problem. And I really want to make sure that you get as much as possible out of this course, so if you skip ahead or skip around, you are probably not going to get as much out of it as you could if you followed it step by step. 

- Along with that you are not going to get a lot of it if you just watch the video. Watchin the videos is again one of the steps you should take, but if you just watch thee videos its not a good way for you to assimilate the knowledge. 

- So, what I wanted you to do is to pause the video and have like a notebook with you and just try and code up the problem first. Even if you are not sure, take a moment and experiment a little bit and see if you can make some sort of progress nd thats gonna help your brain to really make the connections in a more meaningful way then you would if you just watch the video. 

- Do the assignments before moving on. Thats why we have he assignments. You have the assignments so that you can do the ssignments so that you can contnue this process of really reinforcing the knowledge that you are learning.

- They are either the problems that you really need to know or very closely related and will help you practise those same techniques so that you can get these practise as you go through. 

- **What are your goals?**
   - Important to know what you want out of the course so that you can achieve it. 
   - Set SMART goals
        - Specific
        - Measurable 
        - Achievable
        - Relevant 
        - Time-bounded
    - So a bad example of a goal would be I will learn recursion. 
    - A good example of a goal would be I wanna be able to solve the knapsack problem without looking at any references by \<somedate>

## Key Recursive Concepts <a name="key-concepts"></a>

- Even if you know a lot about the recursion, I would recommend that you go through this video because its oing to cover everything and its going to make sure that we are just all on the same page. 

- So, to start off, lets talk about how to identfy when to use recursion. 

- **Identify when to use recursion:**
    - The problem with identifying when to use recursion is that there is no "one size fits all" answer solution. Recursion is such a broad topic. Any problem that can be solved recursively can be solved iteraively as well. So, there is no case where its like we need to use recursion. 

    - But, we can still ask some questions: (Our goal here would be to figure out when would it be easier for us to use recursion and not necessarily when recursion is the only option). 

        1. Does the problem breakdown into sub-problems [by the definition of the recursion]

        2.  Could we solve the problem with an arbitrary number of nestedfor loops?[Lets say I anted you to find all possible combinations for 6 digit numbers. You could do 6 nested for loops. What if I say all possible n digit numbers, it becomes realy difficult.] So, if you see a problem which can be solved using an arbitrary number of for loops, you can also solve it using recursion instead. 

        3. Does the problem fit into one of our recursive pattterns. 

        4. Is it easier to solve it recursively than iteratively? 

- **Basic Recursive Approach:**

    1. Find the base case
        - Start with small examples. [so, like what woud the solution be if the input size is zero]

        - Think about the return type. [This is a really good heuristic because, if your function is going to return a list, then your base case has to return a list of some sort. But you wanna be careful that you match the base case to the return type]

    2. Find the recursive step:
        - The subproblem should be of the same form as the original problem. [You should expect the recursive call to return the same thing as the outer function]
        - Do the recursive steps converge to the base case?

- **How do we return the results of recursive function:**

    1. Can print the result
    
    2. Return results recursively:
        - Store resuts in a global variable.
        - Store the results to a passed variable in our recursive function. 
        - Build up the results as we return. 

    - Example of passing attendance sheet in a class.
    - This is useful for dynamic programming. It allows us to separate out the subproblems from everything else. 





Backtracking:

1. An algorithm that "backtracks" when the current path is invalid. 

2. Useful when a partial solution affects validity of complete soutions.


Recursion vs. Iteration
1. All recursive problems can be solved iteratively and vice versa. However, one appproach maybe much easier.
2. If equal time complexity, iteraive is usually faster.
3. Converting recursive to iteraive:
    a. Not always easy to do. No 'one size fits all' approach
    b. Can you iteratively compute large sub problems?
    c. Can you emulate the recursive stack using other data structures?



### 3. Six Recursive Pattterns:

- Each pattern provides a basic framework.
- Once, you understand the framework, you can fit all problems into one of the frameworks. 
- Pattern+Examples.
- These patterns overlap extensively - find the pattern that works for you. 
- All patterns are based in the recursion principles. 

1. Iteration

- Iterate over list/array using recursion. 
- Rarely used except for simplying code. 
- Examples:
    - Print a Linked LIst in reverse order.
    - Factorial
    - Any time when you might use a for loop. 

2. Breaking into sub-problems

- There are some problems where they are legitimately easy to solve by finding the subproblems. 
- Technically all recursive problems need breaking into sub-problems, but many are not obvious (hence other patterns)
- Examples:
    - Towers of Hanoi
    - Fibonacci

3. Selection (equivalent to finding all combinations)

- To select a set of values. Fundamentally, problems that can be solved by finding all valid combinations. 
- Brute force - find and validate every combination. 
- You can also optimise by validating as we go [or] by using backtracking
- Examples
    - Knapsack Problem
    - Wordbreak
    - Phonespell
    - N Queens

4. Ordering (equivalent to permuations)

- Similar to selection except order matters. 
- Brute force - fin all permutations and validate which is best/matches our conditions. 
- Examples:
    - Find all permutations
    - Find all N digit number whose digits sum to a specific value. 
    - Wordsquares

5. Divide and Conquer

- Similar to sub problems. It is saying how can I reduce my input size to 1 to ultimately get to a solution. Whereas D&C is like how do I split my input into parts and combine the results. 
- Can we solve the problm for ach half of the onput and easily combine the results?
- Common with searching, sorting and trees. 
- Examples:
    - MergeSort
    - Generate all BSTs for a set of items 
    - Find all valis parenthesis

6. Depth First  Search.

- Common technique with tree/graph data structure.
- Can be used for many diferent recursive problems not confined only to trees/graphs
- Examples
    - Searching in a tree.
    - Probability of a knight on a chessboard. 
    - Many recursive problems if framed properly. 












Understand any recursive code:
- Critical to be able to trace through the recursive code. 
- Understand the order of operations - This is annoying because the order of code on the screen is not the same as the order of code being executed. You need to know what order operations are happenning and how they are interacting with each in order to figureout whats going on effetively.
- Drawing recursive tree and tracking variables is crucial. Its gonna help us to keep track of where we are and help us to understand the code logic.
    - Do not lose track of where you are. 

Why do we have to understand the code?
- Exposure to real world problems is the best way to get used to the patterns. 
- Everyone will have different idea to approach the problems and different ways to look at the problem and everyone will do a slightly different things. So, looking at what everyone is doing is huge. Its basically like theme and variation. 
    - Very helpful to variations that others have done. 
    - Need to be able to truly understand the code and examples in this course. 







Irish Start Ups Office Hours | https://officehoursirelandstartupsjune.splashthat.com/
AWS Startup Garage | https://www.meetup.com/AWS-Startup-Garage/




Tail recursion:

It is an optimisation to save space by not having to store stack frames.  And we do that by moving the recursive calls to the end of the function. 




=====================================================

Module 2: Iteration

- Replace for loops with recursion
- Enables us to iterate over the data using recursion. 
- Rarely better than iterative code, but ocasssionally useful. 
- Helpful when we wanted to access DS in reverse. 

Iterating over an array:
- Recursively print all elements in an array. Write functions to print the array in order and in reverse order. 
- What is our iterative step? Ho are we incrementing?
    - i=i+1
    - Can be helpful if you have a look at iteraative code first
- What direction do we want to iterate - printing over array in order and out of order.

- What do we do if we actually want to return the values?
    - An example can be to sum up all the values in the array.
    - Remember our different return strategies?
        - We can build up the results as we return or use a passed variable.

- Try looking into the iterative approach first and work backwards from it. 