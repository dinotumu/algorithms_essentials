# imports


# init variables


# function definitions

def recursive_passed(i, secondary_list, primary_list):
    tertiary_list = secondary_list[:]
    tertiary_list.append(i)
    # print(tertiary_list)

    if i >= 3:
        primary_list.append(tertiary_list)
        return 1

    print("In call f(", i, ") Calling f(", i+1, ")")
    rec_one = recursive_passed(i+1, tertiary_list, primary_list)

    print("In call f(", i, ") Calling f(", i+2, ")")
    rec_two = recursive_passed(i+2, tertiary_list, primary_list)

    # print(rec_one + rec_two)
    return rec_one + rec_two + 1

def main():
    """ Main entry point of the app """
    primary_list = []
    print(recursive_passed(0, [], primary_list))
    print(primary_list)

    # Count leaf nodes - simply add in return

    # Count all nodes or all function calls - Add +1 in return

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()