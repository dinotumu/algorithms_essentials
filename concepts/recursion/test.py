# stack implementation using python lists
stack = [] 

# append() function to push element in to the stack 
stack.append('1') 
stack.append('2') 
stack.append('3')
stack.append('4') 

print('Initial stack') 
print(stack) 

# pop() fucntion to pop element from stack in LIFO order 
print('\nElements poped from stack:') 
print(stack.pop()) 
print(stack.pop()) 
print(stack.pop()) 

print('\nStack after elements are poped:') 
print(stack) 

# uncommenting print(stack.pop()) 
# will cause an IndexError 
# as the stack is now empty 