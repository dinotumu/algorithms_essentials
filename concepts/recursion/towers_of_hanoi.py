#!/usr/bin/python3

"""
Module Docstring
"""

__author__ = "Dinesh Tumu"
__version__ = "0.1.0"
__license__ = "MIT"


# imports


# init variables


# function definitions

def TowerOfHanoi(n , from_rod, to_rod, aux_rod): 
    if n == 1: 
        print ("Move disk 1 from rod",from_rod,"to rod",to_rod )
        return

    print("In call f(", n, ") Calling f(", n-1, from_rod, aux_rod, to_rod, ")" )
    TowerOfHanoi(n-1, from_rod, aux_rod, to_rod) 

    print("Move disk",n,"from rod",from_rod,"to rod",to_rod )

    print("In call f(", n, ") Calling f(", n-1, aux_rod, to_rod, from_rod, ")")
    TowerOfHanoi(n-1, aux_rod, to_rod, from_rod) 
	


def recursive(i):
    if i >= 5:
        return

    print("In call f(", i, ") Calling f(", i+1, ")")
    recursive(i+1)

    print("In call f(", i, ") Calling f(", i+2, ")")
    recursive(i+2)

    return

def main():
    """ Main entry point of the app """
    n = 2
    TowerOfHanoi(n, 'S', 'D', 'a')


if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()